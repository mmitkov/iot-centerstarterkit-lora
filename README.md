**IoT-CenterStarterKit-LoRA**



**Information:**

IoT-CenterStarterKit-LoRa is a low-cost solution device intended for research and develop every concept of LoRaWAN®. It can be used as development board by both experienced professionals and beginner-level engineers which intend to expand their knowledge in the field of wireless communications. The device is designed as a flexible platform which combines wireless communication at 868 МHz through LoRa protocol as well as most commonly used wired interfaces and a bunch of onboard sensors. The installed expansion headers allow the usage of SPI, I2C and UART. The user-device feedback is implemented by a button and a LED. IoT-CenterStarterKit-LoRa comes with reprogrammed firmware for measuring the ambient temperature and humidity. The device supports connectivity with the most common platforms for data visualization and analyse via [TheThingsNetwork](https://www.thethingsnetwork.org/) network layer support. IoT platforms like [IoT-Center](https://iot-center.de/) or [Cayenne-myDevices](https://accounts.mydevices.com/auth/realms/cayenne/protocol/openid-connect/auth?response_type=code&scope=email+profile&client_id=cayenne-web-app&state=9g75nUE046f1xJCIv2r0odlBdlHn0LoG5KzhTfHZ&redirect_uri=https%3A%2F%2Fcayenne.mydevices.com%2Fauth%2Fcallback) could be easily set-up by xConfig software.


**Use precautions:**

- IoT-CenterStarterKit-LoRa is powered by 3 x AAA batteries and it should be used at home or office as a hobby electronics device.

- The device is using 868MHz frequency band for LoRa communication.
- The batteries are not included!

- The enclosure is optional.

- Sensors presence may differ due to users choices.

**Sensors specification:**

| magnitude | sensor | resolution | accurancy | range |
| ------ | ------ | ------ | ------ | ------ |
| humidity | Si7020-A20 | 12 bits | ±4 %rH |  0 ~ 100 %rH |
| temperature | Si7020-A20 | 14 bits | ±0.4 °C | –40°C ~ 125 °C|
| ambient light | TSL25911FN | 16 bits | - | 188μ ~ 88k Lux|
| pressure | MPL3115A2 | 20 bits | ±0.4 kPa | 50k ~ 150 kPa |
| temperature | MPL3115A2 | 12 bits | ±3 °C | –40°C ~ 85 °C |
| 3D accelerometer | BMI160 | 16 bits | ±150 mg | ±2/±4/±8/±16 g |
| 3D gyroscope | BMI160 | 16 bits | ±3 °/s | 125/250/500/1000/2000 °/s |
| eCO2 | CCS811 | 16 bits | - | 400 ~29206 ppb |
| eTVOC | CCS811 | 16 bits | - | 0 ~ 32768 ppb |

**Extension headers pinout**

| Pin No | I2C(J3) | UART(J4) | SPI(J6) |
| ------ | ------ | ------ | ------ |
| 1 | SDA | Tx | MISO |
| 2 | SCL | Rx | MOSI |
| 3 | D I/O | Vdd | SCK |
| 4 | Vdd | GND | CS/SS |
| 5 | GND | - | Vdd |
| 6 | - | - | GND |

**Firmware**

- example firmware for first use -> "LoRaWAN.bin"
- the platform is open to be tested with any other custom firmware. Example for LoRa stack could be found at STMicroeletronics website [here.](https://www.st.com/en/evaluation-tools/b-l072z-lrwan1.html#tools-software)

**Software**

xConfig is used for set-up the IoT-CenterStarterKit-LoRa. The software is supported by Linux and Windows OS. It is provided in executable format _(No intallation is required)._

The Mitkov Systems GmbH’s support team will be glad to help and support every difficulty that may occur during the standard usage of IoT-CenterStarterKit-LoRa. Please contact us on: mail@mitkov-systems.de
